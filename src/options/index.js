// Saves options to chrome.storage
function save_options() {
	var blockedUsers = 'USLACKBOT';
	chrome.storage.sync.set({
		blockedUsers: blockedUsers
	}, function() {
		alert("Please refresh your Slack for this to take effect.")
	});
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
	// Use default value blockedUsers = '', onlyBlockDMs = false and enableExtension = true.
	chrome.storage.sync.set({
		blockedUsers: ""
	}, function() {
		alert("Please refresh Slack for this to take effect.")
	});
}
document.getElementById('mute').addEventListener('click', save_options);
document.getElementById('unmute').addEventListener('click', restore_options);
