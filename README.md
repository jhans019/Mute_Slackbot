# Mute Slackbot Responses #
<p align="center">
<img src="icons/icon256.png" width="256"/>
<p align="center">Mute or unmute slackbot responses to prevent them from obscuring messages.</p>
</p>